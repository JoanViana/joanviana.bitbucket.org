Barchel WEB 
=======================

Documentation of Barchel web generated with [phpDocumentor][link_phpDocumentor] 

Documentation: Open your browser at [http://JoanViana.bitbucket.org][link_doc]

Web : Open your browser at [https://barchel.herokuapp.com][link_barchel]

License
----
MIT

Author
-----
Joan Dídac Viana Fons <joanvianafons@gmail.com>

[link_barchel]: https://barchel.herokuapp.com "Barchel web"

[link_phpDocumentor]: https://www.phpdoc.org/

[link_doc]: http://JoanViana.bitbucket.org