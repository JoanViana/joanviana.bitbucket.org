<?php

/**
 * This file contains BreadProductProvider Class which to alter the way of Bread used from SonataProductBundle used.
 *
 * @package JV\Bundle\BarchelBundle\Provider
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

namespace JV\Bundle\BarchelBundle\Provider;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\ProductBundle\Model\BaseProductProvider;
use JMS\Serializer\SerializerInterface;

/**
 * BreadProductProvider Class which to alter the way of Bread used from SonataProductBundle used.
 *
 * @package JV\Bundle\BarchelBundle\Provider
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

class BreadProductProvider extends BaseProductProvider
{
    /**
	 * Enable a modal for “add to basket” product page button
     * 
     * @package JV\Bundle\BarchelBundle\Provider
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     * 
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->setOptions(array(
            'product_add_modal' => true
        ));
    }

    /**
     * Get base controller name
     * 
     * @package JV\Bundle\BarchelBundle\Provider
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     * 
     * @return string
     */
    public function getBaseControllerName()
    {
        return 'JVBarchelBundle:Bread';
    }

    /**
     * This function adds to FormMapper two parameter to the SonataProduct Entity.
     * The columns added are added to the create and edit forms.
     * This function overwrites the corresponding extended class
     * 
     * @package JV\Bundle\BarchelBundle\Provider
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     * 
     * @param FormMapper $formMapper
     * @param boolean $isVariation
     */
    public function buildEditForm(FormMapper $formMapper, $isVariation = false)
    {
        parent::buildEditForm($formMapper, $isVariation);

        $formMapper
            ->with('Product')
                ->add('grams')
                ->add('calories');

        $formMapper->end();
    }
}

