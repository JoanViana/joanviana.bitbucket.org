<?php

/**
 * This file contains LoadNewsData Class which loads News fixtures.
 *
 * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

namespace JV\Bundle\BarchelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

use Sonata\NewsBundle\Model\TagInterface;
use Sonata\NewsBundle\Model\CommentInterface;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * LoadNewsData Class which loads News fixtures.
 * This class is based on Sonata\Bundle\DemoBundle\DataFixtures\ORM\LoadNewsData from Sylvain Deloux <sylvain.deloux@ekino.com>
 *
 * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

class LoadNewsData extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * Object that manages the instantiation of services
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * Sets container
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Returns the Sonata PostManager.
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return \Sonata\NewsBundle\Model\PostManagerInterface
     */
    public function getPostManager()
    {
        return $this->container->get('sonata.news.manager.post');
    }

    /**
     * Returns the Sonata CommentManager.
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return \Sonata\NewsBundle\Model\PostManagerInterface
     */
    public function getCommentManager()
    {
        return $this->container->get('sonata.news.manager.comment');
    }

    /**
     * Returns the Sonata TagManager.
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return \Sonata\CoreBundle\Model\ManagerInterface
     */
    public function getTagManager()
    {
        return $this->container->get('sonata.classification.manager.tag');
    }

    /**
     * Returns the Pool Formatter.
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return Sonata\FormatterBundle\Formatter\Pool
     */
    public function getPoolFormatter()
    {
        return $this->container->get('sonata.formatter.pool');
    }

    /**
     * Returns the order in which fixtures will be loaded
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return integer
     */
    public function getOrder()
    {
        return 7;
    }

    /**
     * Load post given parameters
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @param string $user
     * @param string[] $tags
     * @param string $collection
     * @param \Sonata\MediaBundle\Model\MediaInterface $image
     */
    public function loadPost($user,$tags,$collection,$image=NULL)
    {
        $postManager = $this->getPostManager();
        $commentManager = $this->getCommentManager();
        $faker = $this->getFaker();

        $post = $postManager->create();
        $post->setAuthor($this->getReference($user));
        $post->setCollection($this->getReference($collection));
        $post->setAbstract('Welcome to Barchel Web. Here you can check our activity and buy our products');
        $post->setEnabled(true);
        $post->setTitle('Welcome to Barchel Web');
        $post->setPublicationDateStart($faker->dateTimeBetween('-30 days', '-1 days'));
        $post->setRawContent('<p>Welcome to Barchel Web. Here you can check our activity and buy our products. We are working to keep it real.</p>');
        $post->setContentFormatter('markdown');
        $post->setContent($this->getPoolFormatter()->transform($post->getContentFormatter(), $post->getRawContent()));
        $post->setCommentsDefaultStatus(CommentInterface::STATUS_VALID);
        $post->setImage($image);
        foreach($tags as $tag) {
            $post->addTags($this->getReference($tag));
        }
            $comment = $commentManager->create();
            $comment->setEmail('jenny.hhussad@gmail.com');
            $comment->setName('Jenny');
            $comment->setStatus(CommentInterface::STATUS_VALID);
            $comment->setMessage('If you need some help just give me a call!');
            $comment->setUrl($faker->url);
            $post->addComments($comment);
     
        $postManager->save($post); 
     }

    /**
     * Function which loads News fixtures
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @param ObjectManager $manager
     */
     public function load(ObjectManager $manager)
    {
        $this->loadPost('user_editor',array('tag_news','tag_article'),'collection_article',$this->getReference('media_barchel'));
    }

    /**
     * Get Faker Generator
     *
     * @package JV\Bundle\BarchelBundle\DataFixtures\ORM
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return \Faker\Generator
     */
    public function getFaker()
    {
        return $this->container->get('faker.generator');
    }
}

