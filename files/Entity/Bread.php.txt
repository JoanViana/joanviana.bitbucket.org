<?php

/**
 * This file contains Bread Class which provides the Bread Entity.
 *
 * @package JV\Bundle\BarchelBundle\Entity
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

namespace JV\Bundle\BarchelBundle\Entity;

use Application\Sonata\ProductBundle\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bread Class which provides the Bread Entity.
 * This class extends Product Class from SonataProductBundle
 *
 * @package JV\Bundle\BarchelBundle\Entity
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 *
 * @ORM\Entity
 * @ORM\Table(name="product__product")
 */
class Bread extends Product
{

    /**
     * Product weight in grams
     *
     * @ORM\Column(type="integer", name="grams")
     * @var integer $grams
     */
    protected $grams;

    /**
     * Product calories per 100 grams
     *
     * @ORM\Column(type="integer", name="calories")
     * @var integer $calories
     */
    protected $calories;

    /**
     * Get grams
     *
     * @package JV\Bundle\BarchelBundle\Entity
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return integer $grams
     */
    public function getGrams()
    {
        return $this->grams;
    }

    /**
     * Set grams
     *
     * @package JV\Bundle\BarchelBundle\Entity
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @param int $grams
     */
    public function setGrams($grams)
    {
        $this->grams = $grams;
    }

    /**
     * Get calories
     *
     * @package JV\Bundle\BarchelBundle\Entity
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @return integer $calories
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set calories
     *
     * @package JV\Bundle\BarchelBundle\Entity
     * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
     * @version 0.1 - Date: 20/05/215
     * @copyright 2016 Barchel
     * @license MIT
     *
     * @param int $calories
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;
    }
}
