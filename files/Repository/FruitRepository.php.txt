<?php

/**
 * This file contains FruitRepository Class which provides the Repository for the Fruit Entity.
 *
 * @package JV\Bundle\BarchelBundle\Repository
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

namespace JV\Bundle\BarchelBundle\Repository;

use Sonata\ProductBundle\Repository\BaseProductRepository;

/**
 * FruitRepository Class which provides the Repository for the Fruit Entity.
 * This class extends BaseProductRepository from SonataProductBundle
 *
 * @package JV\Bundle\BarchelBundle\Repository
 * @author Joan Dídac Viana Fons <joanvianafons@gmail.com>
 * @version 0.1 - Date: 20/05/215
 * @copyright 2016 Barchel
 * @license MIT
 */

class FruitRepository extends BaseProductRepository
{
}

